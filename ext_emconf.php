<?php

/***************************************************************
 * Extension Manager/Repository config file for ext "ke_search_hooks".
 *
 * Auto generated 28-04-2016 14:26
 *
 * Manual updates:
 * Only the data in the array - everything else is removed by next
 * writing. "version" and "dependencies" must not be touched!
 ***************************************************************/

$EM_CONF[$_EXTKEY] = array (
	'title' => 'BossharTong Faceted Search Hooks and Indexers',
	'description' => 'Bosshartong hooks and indexers for ke_search.',
	'category' => 'backend',
	'shy' => 0,
	'version' => '1.0.2',
	'dependencies' => '',
	'conflicts' => '',
	'priority' => '',
	'loadOrder' => '',
	'module' => '',
	'state' => 'stable',
	'uploadfolder' => 0,
	'createDirs' => '',
	'modify_tables' => '',
	'clearcacheonload' => 0,
	'lockType' => '',
	'author' => 'Nando Bosshart',
	'author_email' => 'typo3@bosshartong.ch',
	'author_company' => 'BossharTong GmbH',
	'CGLcompliance' => NULL,
	'CGLcompliance_note' => NULL,
	'constraints' => 
	array (
		'depends' => 
		array (
			'ke_search' => '2.5.0',
		),
		'conflicts' => 
		array (
		),
		'suggests' => 
		array (
		),
	),
);

?>