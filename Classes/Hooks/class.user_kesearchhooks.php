<?php


/***************************************************************
 * This class is an example for a custom indexer for ke_seach,
 * a faceted search extension for TYPO3.
 * Please use it as a kickstarter for your own extensions.
 * It implements a simple indexer for tt_news (although
 * there's already one implemented in ke_search itself).
 ***************************************************************/

use \TYPO3\CMS\Core\Utility\DebugUtility, \TYPO3\CMS\Core\Utility\GeneralUtility;
use Bosshartong\Base\Utility\Sanitize;

class user_kesearchhooks
{


    /**
     * Adds the custom indexer to the TCA of indexer configurations, so that
     * it's selectable in the backend as an indexer type when you create a
     * new indexer configuration.
     *
     * @param array $params
     * @param \TYPO3\CMS\Backend\Form\FormEngine $pObj
     */
    function registerIndexerConfiguration(&$params, $pObj)
    {

        /**
         * contacts
         */
        $contactIndexer = \Bosshartong\BotoKesearch\Indexer\Types\ContactOverview::getInstanz();
        $contactIndexer->registerIndexerConfiguration($params, $pObj);


    }

    /**
     * Custom indexer for ke_search
     *
     * @param   array $indexerConfig Configuration from TYPO3 Backend
     * @param   \tx_kesearch_indexer $indexerObject Reference to indexer class.
     *
     * @return  string Output.
     * @author  Christian Buelter <buelter@kennziffer.com>
     * @since   Fri Jan 07 2011 16:01:51 GMT+0100
     */
    public function customIndexer(&$indexerConfig, &$indexerObject)
    {

        $content = '';

        /**
         * contacts
         */
        if ($indexerConfig['type'] == 'contactoverview')
        {
            $contactIndexer = \Bosshartong\BotoKesearch\Indexer\Types\ContactOverview::getInstanz();
            $content .= $contactIndexer->doIndex($indexerConfig, $indexerObject);

            return $content;
        }
        /**
         * next content type...
         */


    }


    /**
     *
     * @param array $tempMarkerArray
     * @param array $row
     * @param tx_kesearch_lib $pObj
     */
    public function additionalResultMarker(array &$tempMarkerArray, array $row, tx_kesearch_lib $pObj)
    {
        $settings = $GLOBALS['TSFE']->tmpl->setup['plugin.']['tx_bosshartongkesearch.']['settings.'];

        //        if ($row['type'] == 'page')
        //        {
        //            $indexer = \Bosshartong\BotoKesearch\Indexer\Types\Page::getInstanz();
        //            $indexer->additionalResultMarker($tempMarkerArray, $row, $settings);
        //        }


        if ($row['type'] == 'contactoverview')
        {
            $indexer = \Bosshartong\BotoKesearch\Indexer\Types\ContactOverview::getInstanz();
            $indexer->additionalResultMarker($tempMarkerArray, $row, $settings);
        }


        // if ($row['type'] == 'contenttextimage')
        // {
        //     $indexer = \Bosshartong\BotoKesearch\Indexer\Types\Contenttextimage::getInstanz();
        //     $indexer->additionalResultMarker($tempMarkerArray, $row, $settings);;
        // }

    }




}

?>
