<?php


/***************************************************************
 * This class is an example for a custom indexer for ke_seach,
 * a faceted search extension for TYPO3.
 * Please use it as a kickstarter for your own extensions.
 * It implements a simple indexer for tt_news (although
 * there's already one implemented in ke_search itself).
 ***************************************************************/

use \TYPO3\CMS\Core\Utility\DebugUtility, \TYPO3\CMS\Core\Utility\GeneralUtility;

class user_kesearchpagebrowser
{

function pagebrowseAdditionalMarker(&$params, $pObj) {

    // simpler replacement for list-class (12/2016):
    $params['pages_list'] = preg_replace("/^(<ul)/", "$0 class=\"pagination\"", $params['pages_list']);
    //    var_dump($params);
}
}
?>
