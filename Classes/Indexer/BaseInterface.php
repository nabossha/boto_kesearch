<?php
namespace Bosshartong\BotoKesearch\Indexer;



use TYPO3\CMS\Core\Utility\GeneralUtility;

interface BaseInterface
{

    /**
     * Adds the custom indexer to the TCA of indexer configurations, so that
     * it's selectable in the backend as an indexer type when you create a
     * new indexer configuration.
     *
     * @param array $params
     * @param \TYPO3\CMS\Backend\Form\FormEngine $pObj
     */
    public function registerIndexerConfiguration(&$params, $pObj);

    /**
     * @param array $tempMarkerArray
     * @param array $row
     * @param array $settings
     *
     */
    public function additionalResultMarker(array &$tempMarkerArray, array $row, $settings);

    /**
     * @param array $indexerConfig
     * @param \tx_kesearch_indexer $indexerObject Reference to indexer class. $indexerObject
     * @return string
     */
    public function doIndex($indexerConfig, $indexerObject);

    /**
     * @return object
     */
    static public function getInstanz();


}