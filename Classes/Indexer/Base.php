<?php
namespace Bosshartong\BotoKesearch\Indexer;



use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;

abstract class Base implements BaseInterface
{

    protected $retValContent = '';

    /**
     * @var \TYPO3\CMS\Extbase\Object\ObjectManager
     */
    protected $objectManager = null;

    /**
     * @var \TYPO3\CMS\Extbase\Service\FlexFormService
     */
    protected $flexFormService = null;

    /**
     * @var \TYPO3\CMS\Core\Database\QueryGenerator
     */
    protected $queryGen = null;

    /**
     * @var \TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer
     */
    protected $cObj = null;

    /**
     * @var \TYPO3\CMS\Core\Resource\FileRepository
     */
    protected $fileRepository = null;


    /**
     * @var array
     */
    public $firstLevelMap = array();

    /**
     * @var array of all german main navigation pids
     */
    protected $mainNavigationPids = array();

    /**
     * @todo to be adapted if more root pids are added
     * @var array of all root pids, started with Germany
     */
    protected $rootPids = array(2);


    public function __construct()
    {
        $this->objectManager = GeneralUtility::makeInstance('TYPO3\CMS\Extbase\Object\ObjectManager');
        $this->flexFormService = $this->objectManager->get('TYPO3\CMS\Extbase\Service\FlexFormService');
        $this->queryGen = $this->objectManager->get('TYPO3\CMS\Core\Database\QueryGenerator');
        $this->cObj = $this->objectManager->get('TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer');
        $this->fileRepository = $this->objectManager->get('TYPO3\CMS\Core\Resource\FileRepository');
        $this->mainNavigationPids = $this->setMainNavigationPids();
    }

    /**
     *
     * @param \tx_kesearch_indexer $indexerObject Reference to indexer class. $indexerObject
     * @param array $indexerConfig
     * @param array $dataToIndex
     * @param array $recordTtContent
     * @param string $tags
     * @param string $params sample: $params .= '&param=' . 'paramValue';
     */
    protected function doIndexItem($indexerObject, $indexerConfig, $dataToIndex, $recordTtContent, $tags = '', $params=''){

        $targetpid = $recordTtContent['pid'];
        $sys_language_uid = $recordTtContent['sys_language_uid'];

        /*
         * sample: $params .= '&pamam=' . 'paramValue';
         */

        // additionalFields are very important for reindexing record identification
        $additionalFields = array(
            'sortdate' => $dataToIndex['modifiedAt'],
            'orig_pid' => $recordTtContent['pid'],
            'orig_uid' => (string) $dataToIndex['obj_uid'],
        );

        // ... and store the information in the index
        $indexerObject->storeInIndex($indexerConfig['storagepid'],   // storage PID
            $dataToIndex['title'],                         // record title
            $indexerConfig['type'],         // content type
            $targetpid,    // target PID: where is the single view?
            $dataToIndex['fullcontent'],
            // indexed content, includes the title (linebreak after title)
            $tags,                          // tags for faceted search
            $params,                        // typolink params for singleview
            $dataToIndex['abstract'],
            // abstract; shown in result list if not empty
            $sys_language_uid,    // language uid
            $recordTtContent['starttime'],           // starttime
            $recordTtContent['endtime'],             // endtime
            $recordTtContent['fe_group'],            // fe_group
            false,                          // debug only?
            $additionalFields               // additionalFields
        );
    }

    /**
     * get all recursive contained pids of given Page-UID
     * regardless if we need them or if they are sysfolders, links or what ever
     *
     * @param string $startingPointsRecursive comma-separated list of pids of recursive start-points
     * @param string $singlePages comma-separated list of pids of single pages
     *
     * @return array List of page UIDs
     */
    protected function getPagelist($startingPointsRecursive = '', $singlePages = '')
    {

        $pageList = '';

        // make array from list
        $pidsRecursive = GeneralUtility::trimExplode(',', $startingPointsRecursive, true);
        $pidsNonRecursive = GeneralUtility::trimExplode(',', $singlePages, true);

        // add recursive pids
        foreach ($pidsRecursive as $pid)
        {
            $pageList .= $this->queryGen->getTreeList($pid, 99, 0, '1=1') . ',';
        }

        // add non-recursive pids
        foreach ($pidsNonRecursive as $pid)
        {
            $pageList .= $pid . ',';
        }

        // convert to array
        $pageUidArray = GeneralUtility::trimExplode(',', $pageList, true);

        return $pageUidArray;
    }

    protected function getFlexformData($piFlexform)
    {
        $flexFormData = $this->flexFormService->convertFlexFormContentToArray($piFlexform);

        return $flexFormData;
    }

    /**
     * @param \array $pageList
     * @param \string $CType
     * @param \string $list_type
     *
     * @return array
     */
    protected function findContentsByCType($pageList, $CType, $list_type = '')
    {

        $rows = array();

        if($pageList){
            // get all the entries to index
            // don't index hidden or deleted elements, BUT
            // get the elements with frontend user group access restrictions
            // or time (start / stop) restrictions.
            // Copy those restrictions to the index.
            $fields = '*';
            $table = 'tt_content ';
            // in PIDS
            $where = 'pid IN (' . implode(',', $pageList) . ') ';
            // pages are not hidden or deleted
            $where .= 'AND pid IN (SELECT uid FROM pages WHERE `deleted`=0 AND `hidden`=0)';
            $where .= 'AND hidden = 0 AND deleted = 0 ';
            $where .= 'AND CType LIKE \'' . $CType . '\' ';

            if($list_type){
                $where .= 'AND list_type LIKE \'' . $list_type . '\' ';
            }

            $groupBy = '';

            $orderBy = '';
            $limit = '';
            $rows = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows($fields, $table, $where, $groupBy, $orderBy, $limit);
        }

        return $rows;
    }

    protected function findInlineElementsByTtContent($table, $uidTtContent)
    {
        // get all the entries to index
        // don't index hidden or deleted elements, BUT
        // get the elements with frontend user group access restrictions
        // or time (start / stop) restrictions.
        // Copy those restrictions to the index.
        $fields = '*';
        $where = 'tt_content = ' . (integer) $uidTtContent . ' AND pid IN (';
        $where .= 'SELECT uid FROM pages WHERE `deleted` =0 AND  `hidden` =0';
        $where .= ')';
        $where .= 'AND hidden = 0 AND deleted = 0 ';

        $groupBy = '';

        $orderBy = '';
        $limit = '';
        $rows = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows($fields, $table, $where, $groupBy, $orderBy, $limit);

        return $rows;
    }

    protected function findImageByRelation($uid, $tablename, $fieldname)
    {

        $image = false;
        // makeInstanceTYPO3::CMS::Core::Utility::GeneralUtility

        $fileObjects = $this->fileRepository->findByRelation($tablename, $fieldname, $uid);

        $files = array();
        foreach ($fileObjects as $fileObject)
        {
            $image = $fileObject->toArray();
            break;
        };

        return $image;
    }

    /**
     * return page attributes
     *
     * @param $pid
     *
     * @return integer
     */

    public function getPageAttributesByPid($pid)
    {
        $fields = '*';
        $table = 'pages';
        $where = 'uid = ' . intval($pid) . ' ';
        $where .= 'AND hidden = 0 AND deleted = 0 ';

        $groupBy = '';
        $orderBy = '';
        $row = null;

        $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetSingleRow($fields, $table, $where, $groupBy, $orderBy);
        $GLOBALS['TYPO3_DB']->sql_free_result($row);

        return $row;
    }


    private function setMainNavigationPids()
    {
        $fields = 'uid';
        $table = 'pages';
        $where = 'pid IN( ' . implode(',', $this->rootPids) . ') ';
        $where .= 'AND hidden = 0 AND deleted = 0 ';

        $groupBy = '';
        $orderBy = '';

        $rows = $GLOBALS['TYPO3_DB']->exec_SELECTgetRows($fields, $table, $where, $groupBy, $orderBy);
        $collectedUids = array();

        foreach($rows as $row)
        {
            $collectedUids[] = $row['uid'];
        }

        return $collectedUids;
    }


    /**
     * return pid of Main Navigation
     *
     * @param $pid
     *
     * @return integer
     */
    public function getMainNavigationSublevelEntry($pid)
    {
        $fields = 'pid, title';
        $table = 'pages';

        $groupBy = '';
        $orderBy = '';
        $row = null;
        $givenPid = $pid;
        $collectedPids = array();

        while(!in_array(intval($pid), $this->mainNavigationPids))
        {
            if(in_array($pid, $this->firstLevelMap))
            {
                return array('title' => $this->firstLevelMap['pid']);
            }
            else
            {
                $where = 'uid = ' . intval($pid) . ' ';
                $row = $GLOBALS['TYPO3_DB']->exec_SELECTgetSingleRow($fields, $table, $where, $groupBy, $orderBy);
                $pid = intval($row['pid']);
                $collectedPids[] = $pid;
                $GLOBALS['TYPO3_DB']->sql_free_result($row);
            }
        }

        foreach($collectedPids as $collectedPid)
        {
            if(!in_array($collectedPid, $this->firstLevelMap))
            {
                $this->firstLevelMap[$collectedPid] = $row['title'];
            }
        }

        if(!in_array($givenPid, $this->firstLevelMap))
        {
            $this->firstLevelMap[$givenPid] = $row['title'];
        }

        return $row;
    }

    /**
     * Adds the custom indexer to the TCA of indexer configurations, so that
     * it's selectable in the backend as an indexer type when you create a
     * new indexer configuration.
     *
     * @param array $params
     * @param \TYPO3\CMS\Backend\Form\FormEngine $pObj
     */
    public function registerIndexerConfiguration(&$params, $pObj){

        echo 'Error: registerIndexerConfiguration class method is not imlemented..';
        exit;

        return NULL;
    }

    /**
     * @param array $tempMarkerArray
     * @param array $row
     * @param array $settings
     *
     */
    public function additionalResultMarker(array &$tempMarkerArray, array $row, $settings){
        echo 'Error: additionalResultMarker class method is not imlemented..';
        exit;

        return NULL;
    }

    /**
     * @param array $indexerConfig
     * @param \tx_kesearch_indexer $indexerObject Reference to indexer class. $indexerObject
     * @return string
     */
    public function doIndex($indexerConfig, $indexerObject){
        echo 'Error: doIndex class method is not imlemented..';
        exit;

        return NULL;
    }



    /**
     * @return \Bosshartong\BosshartongKesearch\Indexer\Types\Page object
     */
    static public function getInstanz(){

        echo 'Error: getInstanz class method is not imlemented..';
        exit;

        return NULL;
    }


    /**
     * @param array $indexerConfig
     * @return string
     */
    protected function retValAddTitle($indexerConfig){
        $this->retValContent .=  '<b>Indexer: "' . $indexerConfig['title'] . '"</b>';
    }

    /**
     * @param array $indexerConfig
     * @return string
     */
    protected function retValAddEmptyRow(){
        $this->retValContent .= '<br />';
    }

    /**
     * @param array $indexerConfig
     * @return string
     */
    protected function retValAddRow($val){
        $this->retValContent .= '<br />' . $val;
    }

    protected function getRetValContent(){
        return '<p>' . $this->retValContent . '</p>';
    }
}