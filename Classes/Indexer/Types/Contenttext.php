<?php
namespace Bosshartong\BosshartongKesearch\Indexer\Types;


use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class Contenttext extends \Bosshartong\BosshartongKesearch\Indexer\Base
{



    public function __construct()
    {

    }


    /**
     * @param array $tempMarkerArray
     * @param array $row
     * @param array $settings
     *
     */
    public function additionalResultMarker(array &$tempMarkerArray, array $row, $settings)
    {
        $uidPage = (int) $row['targetpid'];
        $tempMarkerArray['image'] = $this->getPageImage($uidPage, $settings);

        //get detail-link-url
        $cObj = t3lib_div::makeInstance('TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer');
        //url in title has to be replaced as we want to jump to the content-id-anchor
        $detailUrl = $cObj->typoLink_URL(array('parameter' => intval($row['targetpid']))) . '?#c' . $row['orig_uid'];
        $tempMarkerArray['detail_url'] = $detailUrl;

        $dom = new DOMDocument();
        @$dom->loadHTML(utf8_decode($tempMarkerArray['title']));

        foreach ($dom->getElementsByTagName('a') as $item)
        {
            $item->setAttribute('href', $detailUrl);

            $innerDom = new DOMDocument();
            @$innerDom->loadHTML($tempMarkerArray['title']);

            $tempMarkerArray['title'] = $dom->saveHTML($item);
        }
    }


    /**
     * @return \Bosshartong\BosshartongKesearch\Indexer\Types\Contenttext object
     */
    static public function getInstanz(){
        return GeneralUtility::makeInstance('Bosshartong\\BosshartongKesearch\\Indexer\\Types\\Contenttext');
    }

}