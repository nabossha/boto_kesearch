<?php
namespace Bosshartong\BotoKesearch\Indexer\Types;


use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;

class Page extends \Bosshartong\BotoKesearch\Indexer\Base
{



    public function __construct()
    {

    }


    /**
     * @param array $tempMarkerArray
     * @param array $row
     * @param array $settings
     *
     */
    public function additionalResultMarker(array &$tempMarkerArray, array $row, $settings)
    {
        $uidPage = (int) $row['targetpid'];
        $tempMarkerArray['image'] = $this->getPageImage($uidPage, $settings);
    }

    /**
     * @param $uidPage
     * @param $settings
     *
     * @return string
     */
    protected function getPageImage($uidPage, $settings)
    {
        // read typoscript conf of Store Dummy-Image
        $dummyFile = $settings['result.']['path.']['image.']['dummy.']['content'];
        $imageConf = $settings['result.']['image.'];

        // get the image from the tx_pneuhageproducts_domain_model_index entry
        // and replace marker ###ITEM_IMAGE###
        $fieldname = 'image';
        $table = 'pages';

        $resImage = $this->findImageByRelation($uidPage, $table, $fieldname);

        if ($resImage)
        {
            $imageConf['file'] = $resImage['id'];
            $imageConf['file.']['width'] = '150c';
            $imageConf['file.']['maxW'] = '150';
            $imageConf['file.']['maxH'] = '184';
        }
        else
        {
            $imageConf['file'] = $dummyFile;
        }

        return $this->cObj->IMAGE($imageConf);
    }

    /**
     * @return \Bosshartong\BotoKesearch\Indexer\Types\Page object
     */
    static public function getInstanz(){
        return GeneralUtility::makeInstance('Bosshartong\\BotoKesearch\\Indexer\\Types\\Page');
    }

}