<?php
namespace Bosshartong\BosshartongKesearch\Indexer\Types;


use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;
use DOMDocument;

class BotoContact extends \Bosshartong\BosshartongKesearch\Indexer\Base
{

    const TAGCREWMEMBER = '';


    public function __construct()
    {
        parent::__construct();

    }

    /**
     * @param array $params
     * @param $pObj
     */
    public function registerIndexerConfiguration(&$params, $pObj){

        /**
         * products
         */
        $newArray = array(
            'CE BossharTong Kontakt',
            'boto_contact',
            \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('boto_kesearch') . 'Resources/Public/Icons/ef-crewmember-indexer-icon.gif'
        );
        $params['items'][] = $newArray;

        // Additional TCA changes:
        // vision-components-v2/htdocs/typo3conf/ext/bosshartongkesearch/Configuration/TCA/Overrides/tx_kesearch_indexerconfig.php

    }


    /**
     * @param array $tempMarkerArray
     * @param array $row
     * @param array $settings
     *
     */
    public function additionalResultMarker(array &$tempMarkerArray, array $row, $settings)
    {
        // read typoscript conf of Store Dummy-Image
        $dummyFile = $settings['result.']['path.']['image.']['dummy.']['content'];
        $imageConf = $settings['result.']['crewimage.'];
        $resImage = NULL;

        //get detail-link-url
        //$cObj = GeneralUtility::makeInstance('TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer');
        //$detailUrl = $cObj->typoLink_URL(array('parameter' => intval($row['targetpid'])));
        //$tempMarkerArray['detail_url'] = $detailUrl;

        //get detail-link-url
        $cObj = GeneralUtility::makeInstance('TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer');

        //url in title has to be replaced as we want to jump to the content-id-anchor
        $detailUrl = $cObj->typoLink_URL(array('parameter' => intval($row['targetpid']))) . '#c' . $row['orig_uid'];
        $tempMarkerArray['detail_url'] = $detailUrl;


        $dom = new DOMDocument();
        @$dom->loadHTML(utf8_decode($tempMarkerArray['title']));

        foreach ($dom->getElementsByTagName('a') as $item)
        {
            $item->setAttribute('href', $detailUrl);

            $innerDom = new DOMDocument();
            @$innerDom->loadHTML($tempMarkerArray['title']);

            $tempMarkerArray['title'] = $dom->saveHTML($item);
        }
        // set the custom-teaser so we can parse regular HTML in FLUID-template:
        $tempMarkerArray['customTeaser'] = $row['content'];

        // get product uid from index record
        $uidContent = (int) $row['orig_uid'];

        //show ce image
        $fieldname = 'assets';
        $table = 'tt_content';

        // get image from tt_content
        $resImage = $this->findImageByRelation($uidContent, $table, $fieldname);

        if (!$resImage)
        {
            $uidPage = (int) $row['orig_pid'];
            //show product image
            $fieldname = 'media';
            $table = 'pages';

            // get image from tt_content
            $resImage = $this->findImageByRelation($uidPage, $table, $fieldname);
        }


        if ($resImage)
        {
            $imageConf['file'] = $resImage['id'];
        }
        else
        {
            $imageConf['file'] = $dummyFile;
        }

        $tempMarkerArray['customImageHtml'] = $this->cObj->IMAGE($imageConf);

    }

    /**
     * @param array $indexerConfig
     * @param \tx_kesearch_indexer $indexerObject Reference to indexer class. $indexerObject
     * @return string
     */
    public function doIndex($indexerConfig, $indexerObject){

        // set ke_serach indexer title for result output
        $this->retValAddTitle($indexerConfig);


        $ttContentRows = $this->findAllObjectsToIndex($indexerConfig);


        $resCount = sizeof($ttContentRows);
        $resCountIndexed = 0;
        $resCountCustomerSamplesIndexed = 0;
        $resCounVariantsIndexed = 0;
        $languagesIndexed = array();

        // add info text for indexing result output
        $this->retValAddRow($resCount . ' contents in all languages have been found for indexing.');

        // Loop through the records and write them to the index.
        if ($resCount)
        {
            foreach ($ttContentRows as $recordTtContent)
            {
                $sys_language_uid = $recordTtContent['sys_language_uid'];

                $dataToIndex = $this->buildIndexData($recordTtContent);

                $tags = self::TAGCREWMEMBER;
                $params = '';
                $this->doIndexItem($indexerObject, $indexerConfig, $dataToIndex, $recordTtContent, $tags, $params);


                $languagesIndexed[$sys_language_uid] = 1;
                $resCountIndexed++;
            }

            $this->retValAddRow( $resCountIndexed . ' Products, '
                . $resCountCustomerSamplesIndexed . ' customer samples, '
                . $resCounVariantsIndexed . ' product variants in '
                . sizeof($languagesIndexed) . ' languages have been indexed.');
        }


        return $this->getRetValContent();

    }

    /**
     * @return \Bosshartong\BosshartongKesearch\Indexer\Types\Contenttextimage object
     */
    static public function getInstanz(){
        return GeneralUtility::makeInstance('Bosshartong\\BosshartongKesearch\\Indexer\\Types\\BotoContact');
    }


    /**
     * @param array $indexerConfig
     * @return array
     */
    protected function findAllObjectsToIndex($indexerConfig){
        /**
         * retrive all active site pages
         */
        $startingPointsRecursive = $indexerConfig['startingpoints_recursive'];
        $pageList = $this->getPagelist($startingPointsRecursive);
        $ttContentRows = array();

        $CType = 'boto_contact';
        $list_type  = '';
        $ttContentRowsUnfiltered = $this->findContentsByCType($pageList, $CType, $list_type);

        foreach($ttContentRowsUnfiltered as $ttContentRow){
            //$flexData =  $this->flexFormService->convertFlexFormContentToArray($ttContentRow['pi_flexform']);
            //$ttContentRow['flexform'] = $flexData;
            $ttContentRows[] = $ttContentRow;
        }

        return $ttContentRows;
    }

    /**
     * @param array $recordTtContent
     * @return array
     */
    protected function buildIndexData($recordTtContent){

        $indexData = array(
            'doIndex' => FALSE,
            'title' => '',
            'content' => '',
            'fullContent' => '',
            'abstract' => '',
            'modifiedAt' => 0,
            'obj_uid' => $recordTtContent['uid']
        );

        $indexData['title'] .= $recordTtContent['header'];
        $indexData['content'] .= $recordTtContent['header'] . "\n";
        $indexData['content'] .= $recordTtContent['bodytext'] . "\n";
        $indexData['content'] .= $recordTtContent['subheader'] . "\n";
        $indexData['content'] .= "<link ". $recordTtContent['header_link'] .">".$recordTtContent['header_link'] . "</link>";
        //$indexData['content'] .= strip_tags($product->getDescription()) . "\n";


        $indexData['modifiedAt'] = $recordTtContent['tstamp'];


        if($indexData['content']){
            $indexData['doIndex'] = TRUE;
            $indexData['fullcontent'] = $indexData['content'];
        }

        return $indexData;
    }

}