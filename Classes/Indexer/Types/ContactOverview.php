<?php
namespace Bosshartong\BotoKesearch\Indexer\Types;


use TYPO3\CMS\Core\Utility\DebugUtility;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\StringUtility;
use TYPO3\CMS\Extbase\Utility\DebuggerUtility;
use DOMDocument;

class ContactOverview extends \Bosshartong\BotoKesearch\Indexer\Base
{
    const TAGCONTACT = '#contact#';


    /**
     * @var \Bosshartong\Botocontacts\Domain\Repository\ContactRepository
     */
    private $contactRepository = null;

    public function __construct()
    {
        parent::__construct();

        $this->contactRepository = $this->objectManager->get('Bosshartong\Botocontacts\Domain\Repository\ContactRepository');
    }


    /**
     * @param array $params
     * @param $pObj
     */
    public function registerIndexerConfiguration(&$params, $pObj){

        /**
         * contacts
         */
        $newArray = array(
            'Plugin BoTo Contacts Indexer',
            'contactoverview',
            \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('boto_kesearch') . 'Resources/Public/Icons/products-indexer-icon.gif'
        );
        $params['items'][] = $newArray;

        // Additional TCA changes:
        // vision-components-v2/htdocs/typo3conf/ext/expoformerkesearch/Configuration/TCA/Overrides/tx_kesearch_indexerconfig.php

    }

    /**
     * @param array $tempMarkerArray
     * @param array $row
     * @param array $settings
     *
     */
    public function additionalResultMarker(array &$tempMarkerArray, array $row, $settings)
    {
        // read typoscript conf of Store Dummy-Image
        $dummyFile = $settings['result.']['path.']['image.']['dummy.']['content'];
        $imageConf = $settings['result.']['contactimage.'];
        $resImage = NULL;

        //get detail-link-url
        //$cObj = GeneralUtility::makeInstance('TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer');
        //$detailUrl = $cObj->typoLink_URL(array('parameter' => intval($row['targetpid'])));
        //$tempMarkerArray['detail_url'] = $detailUrl;

        //get detail-link-url
        $cObj = GeneralUtility::makeInstance('TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer');

        //url in title has to be replaced as we want to jump to the content-id-anchor
        //$detailUrl = $cObj->typoLink_URL(array('parameter' => intval($row['targetpid']))) . '#c' . $row['orig_uid'];
        $detailUrl = $cObj->typoLink_URL(array('parameter' => intval($row['targetpid'])));
        $tempMarkerArray['detail_url'] = $detailUrl;


        $dom = new DOMDocument();
        @$dom->loadHTML(utf8_decode($tempMarkerArray['title']));

        foreach ($dom->getElementsByTagName('a') as $item)
        {
            $item->setAttribute('href', $detailUrl);

            $innerDom = new DOMDocument();
            @$innerDom->loadHTML($tempMarkerArray['title']);

            $tempMarkerArray['title'] = $dom->saveHTML($item);
        }
        // set the custom-teaser so we can parse regular HTML in FLUID-template:
        $tempMarkerArray['customTeaser'] = $row['content'];

        // get product uid from index record
        $uidContent = (int) $row['orig_uid'];

        //show ce image
        $fieldname = 'image';
        $table = 'tx_botocontacts_domain_model_contact';

        // get image from tt_content
        $resImage = $this->findImageByRelation($uidContent, $table, $fieldname);

        if ($resImage)
        {
            $imageConf['file'] = $resImage['id'];
        }
        else
        {
            $imageConf['file'] = $dummyFile;
        }

        $tempMarkerArray['customImageHtml'] = $this->cObj->render($this->cObj->getContentObject('IMAGE'), $imageConf);

    }


    /**
     * @param array $indexerConfig
     * @param \tx_kesearch_indexer $indexerObject Reference to indexer class. $indexerObject
     * @return string
     */
    public function doIndex($indexerConfig, $indexerObject){

        // set ke_serach indexer title for result output
        $this->retValAddTitle($indexerConfig);
        $ttContentRows = $this->findAllObjectsToIndex($indexerConfig);

        $resCount = sizeof($ttContentRows);
        $resCountIndexed = 0;

        $languagesIndexed = array();

        // add info text for indexing result output
        $this->retValAddRow($resCount . ' plugins in all languages have been found for indexing.');

        // Loop through the records, get the associated contacts and write them to the index.
        if ($resCount)
        {

            foreach ($ttContentRows as $recordTtContent)
            {
                $sys_language_uid = $recordTtContent['sys_language_uid'];

                /*
                 * settings.selectedPersons
                 */
                $flexForm = $recordTtContent['flexform'];
                $uidContacts = $flexForm['settings']['selectedPersons'];
                $contactRows = explode(",", $uidContacts);

                foreach ($contactRows as $uidContact)
                {
                    /** @var \Bosshartong\Botocontacts\Domain\Model\Contact $contact */
                    $contact = $this->contactRepository->findByUid($uidContact);
                    if($contact){
                        $dataToIndex = $this->buildContactIndexData($contact);
                        $tags = self::TAGCONTACT;
                        $params = '';
                        $this->doIndexItem($indexerObject, $indexerConfig, $dataToIndex, $recordTtContent, $tags, $params);


                    }
                    $languagesIndexed[$sys_language_uid] = 1;
                    $resCountIndexed++;
                }

            }

            $this->retValAddRow( $resCountIndexed . ' Contacts, '
                                . sizeof($languagesIndexed) . ' languages have been indexed.');
        }


        return $this->getRetValContent();

    }


    /**
     * @param array $indexerConfig
     * @return array
     */
    protected function findAllObjectsToIndex($indexerConfig){
        /**
         * retrive all active site pages
         */
        $startingPointsRecursive = $indexerConfig['startingpoints_recursive'];
        $pageList = $this->getPagelist($startingPointsRecursive);
        $ttContentRows = array();

        $CType = 'botocontacts_contact';
        $ttContentRowsUnfiltered = $this->findContentsByCType($pageList, $CType);

        foreach($ttContentRowsUnfiltered as $ttContentRow){
            $flexData =  $this->flexFormService->convertFlexFormContentToArray($ttContentRow['pi_flexform']);
            $ttContentRow['flexform'] = $flexData;
            $ttContentRows[] = $ttContentRow;
        }
        return $ttContentRows;
    }



    /**
     * @param \Bosshartong\Botocontacts\Domain\Model\Contact $contact
     * @return array
     */
    protected function buildContactIndexData($contact){

        $indexData = array(
            'doIndex' => FALSE,
            'title' => '',
            'content' => '',
            'fullContent' => '',
            'abstract' => '',
            'modifiedAt' => 0,
            'obj_uid' => $contact->getUid()
        );

        $email = $contact->getEmail();
        $firstname = $contact->getFirstName();
        $lastname = $contact->getLastName();
        $fullname = $firstname .' ' . $lastname;
        $phone = $contact->getTelephone();
        $indexData['title'] .= $fullname;
        $indexData['content'] .= "<b>".$fullname ."</b>\n" .$contact->getFunctionTitle();
        if($phone)
        {
            $indexData['content'] .= "\n". $phone;
        }
        if($email)
        {
            $indexData['content'] .= "\n". "<link " .$email . ">".$email . "</link>";
        }

        //$indexData['content'] .= strip_tags($product->getDescription()) . "\n";
        //$indexData['modifiedAt'] = $contact->getModifiedAt();

        if($indexData['content']){
            $indexData['doIndex'] = TRUE;
            $indexData['fullcontent'] = $indexData['content'];
        }

        return $indexData;
    }


    /**
     * @return \Bosshartong\BotoKesearch\Indexer\Types\ContactOverview object
     */
    static public function getInstanz(){
        return GeneralUtility::makeInstance('Bosshartong\\BotoKesearch\\Indexer\\Types\\ContactOverview');
    }
}