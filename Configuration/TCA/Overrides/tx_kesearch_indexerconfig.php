<?php

// register Plugin Bosshartong-Products Übersicht Indexer

// exclude targetpid field if type is productsoverview
$GLOBALS['TCA']['tx_kesearch_indexerconfig']['types'][0]['subtype_value_field'] = 'type';
$GLOBALS['TCA']['tx_kesearch_indexerconfig']['types'][0]['subtypes_excludelist'] = array(
    'contactoverview' => 'targetpid',
);

// startingpoints_recursive einblenden
$GLOBALS['TCA']['tx_kesearch_indexerconfig']['columns']['startingpoints_recursive']['displayCond'] .= ',boto_kesearch,contactoverview';


// ================================================= //

// next one here please ...